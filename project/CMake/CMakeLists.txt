# Copyright (c) 2018-2019 Viktor Kireev
# Distributed under the MIT License

cmake_minimum_required(VERSION 3.6)

project(Car CXX)

add_executable(Car ${CMAKE_CURRENT_SOURCE_DIR}/../../src/main.cpp)
set_property(TARGET Car PROPERTY CXX_STANDARD 17)
target_link_libraries(Car Upl pthread)
